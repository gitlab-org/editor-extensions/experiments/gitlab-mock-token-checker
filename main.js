var fs = require("fs");
var http = require("http");
var https = require("https");

var privateKey = fs.readFileSync("./localhost+2-key.pem", "utf8");
var certificate = fs.readFileSync("./localhost+2.pem", "utf8");

var credentials = { key: privateKey, cert: certificate };
var express = require("express");
var app = express();

const DEFAULT_VERSIONS = ["17.4.0", "17.4"];
const VERSION_REGEX = /^(\d+\.\d+)(?:\.\d+(?:-pre)?)/;
const [GITLAB_INSTANCE_VERSION, MILESTONE_VERSION] = process.env.GITLAB_INSTANCE_VERSION?.match(VERSION_REGEX) || DEFAULT_VERSIONS;
if (!MILESTONE_VERSION) {
  console.error(`Invalid $GITLAB_INSTANCE_VERSION ('${GITLAB_INSTANCE_VERSION}') specified. Expected a string like "17.2".`);
  process.exit(1);
}

const ENABLED_FEATURE_FLAGS = process.env.ENABLED_FEATURE_FLAGS?.split(/[, ]+/) || [];
const DISABLED_FEATURE_FLAGS = process.env.DISABLED_FEATURE_FLAGS?.split(/[, ]+/) || [];

const InstanceState = {
  featureFlags: {
    enabled: ENABLED_FEATURE_FLAGS,
    disabled: DISABLED_FEATURE_FLAGS,
  },
  versions: {
    instance: GITLAB_INSTANCE_VERSION,
    milestone: GITLAB_INSTANCE_VERSION,
  },
};

app.use(express.json());

const requireProxyAuth = (handler) => (req, res) => {
  const proxyAuthorization = req.get('proxy-authorization');
  if (!proxyAuthorization) {
    res.set('Proxy-Authenticate', 'Basic realm="mocktokenserver"');
    return res.status(407).json({ error: 'Missing Proxy-Authorization header' });
  }

  return handler(req, res);
};

const handle = (method, path, handler) => {
  if (method === 'get') {
    app.get(path, handler);
    app.get(`/proxied${path}`, requireProxyAuth(handler));
  } else if (method === 'post') {
    app.post(path, handler);
    app.post(`/proxied${path}`, requireProxyAuth(handler));
  } else if (method === 'put') {
    app.put(path, handler);
    app.put(`/proxied${path}`, requireProxyAuth(handler));
  }
};

handle("get", '/-/tanuki/instance', (req, res) => {
  res.json(InstanceState);
});

handle("put", '/-/tanuki/instance', (req, res) => {
  InstanceState.featureFlags = {
    ...InstanceState.featureFlags,
    ...req.body.featureFlags,
  };

  if (req.body.instance_version !== undefined) {
    const [instance, milestone] = req.body.instance_version?.match(VERSION_REGEX) || [];
    if (!milestone) {
      const message = `Invalid instance_version param passed ${req.body.instance_version} specified. Expected a string like "17.2.0-pre".`;
      console.error(message);
      return res.status(400).json({ "error": message });
    }

    InstanceState.versions = { instance, milestone };
  }

  res.json(InstanceState);
});

handle("post", "/api/graphql", (req, res) => {
  console.log("POST /api/graphql");
  console.log(`<<< ${JSON.stringify(req.body, null, 2)}`);
  const op = req.body.operationName || '{NoOperationNameInRequestBody}';

  // Use response JSON by default.
  let handler = () => {
    try {
      const body = require(`./fixtures/api/graphql/${op}/ok.json`);
      return body;
    } catch (err) {
      console.error(`No 200 OK response fixture found for operation: ${op}; err: ${err}`);
      return null;
    }
  };

  // Use the specific javascript handler if possible.
  try {
    handler = require(`./fixtures/api/graphql/${op}/ok.js`);
  } catch (err) {
    console.error(`No handler found for operation: ${op}; err: ${err}`);
  }

  const body = handler(req, InstanceState);
  if (body) {
    return res.json(body);
  }

  console.warn(`>>> Returning error response for operation: ${op}`);
  return res.json({ errors: [{ message: `Unmocked operation called ${op} please update the mock-token-server to handle this operation.`, locations: [{ line: 1, column: 1 } ], path: [`query ${op}`], extensions: { code: "undefinedField", typeName: "AiMessageExtras", fieldName: "additionalContext" }}]});
});

handle("post", "/api/v4/code_suggestions/completions", (req, res) => {
  console.log("/api/v4/code_suggestions/completions");

  res.json({});
});

handle("get", "/api/v4/metadata", (req, res) => {
  console.log("/api/v4/metadata");

  res.json({
    version: GITLAB_INSTANCE_VERSION,
    revision: "58bccc17a89",
    kas: {
      enabled: true,
      externalUrl: "wss://kas.gitlab.com",
      version: `v${GITLAB_INSTANCE_VERSION.replace("-pre", "-rc2")}`,
    },
    enterprise: true
  });
});

handle("get", "/api/v4/personal_access_tokens/self", (req, res) => {
  console.log("/api/v4/personal_access_tokens/self");

  res.json(require('./fixtures/api/v4/personal_access_tokens/self/ok.json'));
});

handle("get", "/api/v4/user", (req, res) => {
  console.log("/api/v4/user");

  res.json(require('./fixtures/api/v4/user/ok.json'));
});

handle("get", "/api/v4/version", (req, res) => {
  console.log("/api/v4/version");
  res.json({ version: GITLAB_INSTANCE_VERSION });
});

// var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

// httpServer.listen(8080);
// https://localhost:8443
httpsServer.listen(8443);

console.log(`Listening to https://localhost:8443`);
console.log(`Simulating ${MILESTONE_VERSION} due to $GITLAB_INSTANCE_VERSION=${GITLAB_INSTANCE_VERSION}.`);
