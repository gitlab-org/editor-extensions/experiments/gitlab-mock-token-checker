const deepcopy = (json) => JSON.parse(JSON.stringify(json));

const handler = (req, { featureFlags, versions }) => {
  const json = deepcopy(require('./17.5.json'));
  if (featureFlags.disabled.includes('duo_additional_context') || !featureFlags.enabled.includes('duo_additional_context')) {
    console.log(`deleting additional context due to featureFlags: ${JSON.stringify(featureFlags)}`)
    json.data.aiMessages.nodes.forEach((node) => {
      delete node.extras.additionalContext;
    });
  }

  return json;
};

module.exports = handler;
