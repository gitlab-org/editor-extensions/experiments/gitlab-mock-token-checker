// Handle the featureFlagsEnabled query from the Language Server:
// https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/blob/f5bd38ee0c/src/common/feature_flags.ts#L21
const handler = (req, { featureFlags, versions }) => {
  // TODO: Define a field resolver for metadata.featureFlags.
  const names = req.body.variables.names;
  if (names) {
    response = {
      data: {
        metadata: {
          featureFlags: names.map((name) => ({
            enabled: featureFlags.enabled.includes(name),
            name,
          }))
        }
      }
    };
  }

  // TODO: Define a field resolver for featureFlagEnabled at the top level.
  const name = req.body.variables.name;
  if (name) {
    response = {
      data: {
        featureFlagEnabled: featureFlags.enabled.includes(name),
      }
    };
  }

  return response;
};

module.exports = handler;
