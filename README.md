#### Usage

Pre-requisites
1. Clone this repository and navigate to the root folder
1. Install https://github.com/FiloSottile/mkcert
   - e.g. `brew install mkcert`
1. Create local certificate pair using `mkcert`
1. Install the root CA with `mkcert -install`
1. Generate a local certificate/key pair with `./make_certificates`
1. Ensure that the paths in `main.js` point to the new files.

Running the app

1. Run `npm install`
1. Run the app with `node main.js`

Building the docker image

```shell
docker build . -t mock-token-server
```

Running the docker image

```shell
docker run --init -it -p 127.0.0.1:8443:8443 mock-token-server
```
