FROM node:21-alpine3.18

RUN apk update
WORKDIR /app
ADD https://github.com/FiloSottile/mkcert/releases/download/v1.4.4/mkcert-v1.4.4-linux-arm64 /app/mkcert

ADD ./scripts/make_certificates ./make_certificates
RUN chmod +x mkcert && ./make_certificates

COPY package.json package-lock.json ./
COPY ./fixtures ./fixtures
COPY main.js ./
RUN npm ci

EXPOSE "8443/tcp"
CMD ["node", "main.js"]
